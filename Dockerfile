FROM denoland/deno:alpine-1.43.5

ENV TZ 'America/New_York'

RUN apk update && \
    apk add --no-cache tzdata curl

EXPOSE 8080
WORKDIR /app
VOLUME [ "/data" ]

HEALTHCHECK --interval=1m --timeout=15s \
    CMD curl --fail http://localhost:8080/api/health || exit 1

COPY . .
RUN deno cache website/deps.ts
CMD ["run", "--allow-env", "--allow-net", "--allow-read", "--allow-write=/data", "website/mod.ts"]
