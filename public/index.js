// https://discord.com/developers/docs/reference#image-formatting
function avatarUrl() {
  const user = JSON.parse(localStorage.getItem('user'));
  return user
    ? `https://cdn.discordapp.com/avatars/${user.id}/${user.avatar}.png`
    : '/assets/discordicon.jpeg';
}

function getCookie() {
  const cookieRow = document.cookie
    ? document.cookie.split('; ').find((row) => row.startsWith('isLoggedIn'))
    : null;
  return cookieRow?.split('=')[1];
}

async function checkAuth() {
  const el = document.getElementById('loginout');
  const cookie = getCookie();

  el.href = cookie ? '/auth/logout' : '/auth/login';
  el.innerHTML = cookie ? 'Logout' : '<b>Login with Discord</b>';
  el.classList.remove('secondary');
  el.classList.add(!cookie ? 'primary' : 'contrast');

  if (cookie) {
    if (!localStorage.getItem('user')) {
      const user = await (await fetch('/api/user')).json();
      localStorage.setItem('user', JSON.stringify(user));
    }
    const avatar = document.getElementById('avatar');
    avatar.src = avatarUrl();
    avatar.removeAttribute('hidden');
  } else {
    if (localStorage.getItem('user')) {
      localStorage.removeItem('user');
    }
  }

  el.setAttribute('aria-busy', 'false');
}

function doBreadCrumbs() {
  const el = document.querySelector('nav[aria-label=\'breadcrumb\']');
  // clear out old entries
  while (el.children[0].children.length > 1) {
    Array.from(el.children[0].children).pop().remove();
  }
  el.children[0].innerHTML += window.location.pathname.split('/').filter((x) =>
    x && x != 'x'
  ).map((x) => `<li><a href="/x/${x}">${x}</a></li>`).join('');
}

document.addEventListener('DOMContentLoaded', () => {
  checkAuth();
  doBreadCrumbs();
});

self.addEventListener('locationchange', doBreadCrumbs);
self.addEventListener('htmx:pushedIntoHistory', doBreadCrumbs);
self.addEventListener('popstate', doBreadCrumbs);
