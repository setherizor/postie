// https://github.com/missive/emoji-mart

window.createReactionMessage = async () => {
  const guildId = document.getElementById('guildId').value;
  if (!guildId) return alert('You have not choosen a guild');

  try {
    // Package up our selections
    const form = document.getElementById('reactionMessageForm');
    const obj = Object.fromEntries(new FormData(form));
    obj.guild = guildId;

    if (!obj.channel) return alert('You have not choosen a channel');
    obj.reactionRoles = {};

    Object.keys(obj)
      .filter((x) => x.includes('role%'))
      .forEach((role) => {
        obj.reactionRoles[role.replace('role%', '')] = JSON.parse(
          atob(obj[role]),
        ).id;
        delete obj[role];
      });

    if (Object.keys(obj.reactionRoles).length == 0) {
      return alert('You did not configure any reactions');
    }

    const response = await fetch('/api/createReactionMessage', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(obj),
    });

    const text = await response.text();

    console.log('message create response: ' + text);

    if (text == 'success') {
      alert('Roll Reaction Message creation was successful!');
      // window.location.reload() // go back to invite page and clear form
    } else if (response.status == 401) {
      alert('You are unauthorized to perform that action');
    }
  } catch (error) {
    console.error(error);
  }
};
(window.onEmojiSelect = (emoji) => {
  // https://abal.moe/Eris/docs/Message#method-addReaction
  emoji.key = btoa(
    encodeURIComponent(
      emoji.src
        ? [
          emoji.name,
          emoji.src
            .split('/')
            .pop()
            .split('.')
            .shift(),
        ].join(':')
        : emoji.native,
    ),
  );

  const el = document.getElementById('reaction-roles');

  const rep = emoji.src
    ? `<img src="${emoji.src}" alt="${
      emoji.native +
      ' ' +
      emoji.name
    }" title="${emoji.name}"/><br><span>${emoji.name}</span>`
    : `<span title="${emoji.name}" style="font-size: 35px;">${emoji.native}</span><br><span>${emoji.name}</span>`;

  el.innerHTML += `
    <pre class="" style="margin: 10px 10px; padding: 10px 10px; display: inline-flex; align-items: center; gap: 10px; justify-content: space-between; width: 100%">
        <div>${rep}</div>
        <select style="width: 50%" type="text" name="${
    'role%' +
    emoji.key
  }" id="${'role%' + emoji.key}">
          <option value="" disabled="" selected="">choose a role...</option>
          ${document.getElementById('role').innerHTML}
        </select>
        <img class="pointer" style="-webkit-filter: invert(100%); filter: invert(100%); float: right" src="/assets/close.png" alt="${
    'remove ' +
    emoji.name
  }" title="${
    'remove ' +
    emoji.name
  }" onclick="this.parentElement.remove()" />
    </pre>`;
}),
  (window.setupPicker = async () => {
    const guildId = document.getElementById('guildId').value;
    const emojiObjs = await (await fetch('/api/emojis/' + guildId)).json();
    const customEmoji = emojiObjs.map((e) => {
      return {
        id: e.name,
        name: e.name,
        skins: [{ src: `https://cdn.discordapp.com/emojis/${e.id}.png` }],
      };
    });

    const custom = [
      {
        id: 'custom',
        name: 'Server Emoji',
        emojis: customEmoji,
      },
    ];

    const pickerOptions = {
      onEmojiSelect,
      onClickOutside: () => picker.remove(),
      custom,
      autoFocus: true,
      categories: [
        'custom',
        'people',
        'nature',
        'foods',
        'activity',
        'places',
        'objects',
        'symbols',
        'flags',
      ],
      noCountryFlags: true,
      skinTonePosition: 'none',
      theme: 'dark',
    };
    window.picker = new EmojiMart.Picker(pickerOptions);
    document.body.appendChild(picker);
  });
