import {
  assert,
  assertEquals,
} from 'https://deno.land/std@0.160.0/testing/asserts.ts';

const test_db_file = Deno.env.get('DB_FILE') + '.test.json';

import { Db } from './db.ts';

Deno.test('Test Init', () => {
  const def = {
    guilds: {},
    authTokens: {},
  };

  const d = new Db(true, test_db_file);

  assertEquals(def, d.data);
});

Deno.test('Test basic get/set', () => {
  const d = new Db(true, test_db_file);

  assert(!d.data.test, 'Should be null');

  const testData = { test: 1 };
  d.set('test', testData);

  assertEquals(d.data.test, testData);

  d.set('test', 3);
  assertEquals(d.data.test, 3, 'overwrite should work');
});

Deno.test('Test basic/advanced deletes', () => {
  const d = new Db(true, test_db_file);
  const testData = { test: 1 };
  assert(!d.data.test, 'Should be null');
  d.set('test', testData);
  assertEquals(d.data.test, testData);
  d.set('test', null);
  assert(!d.data.test, 'Should be null');

  assert(!d.data.test?.very.very.deep, 'Should be null');
  d.set('test.very.very.deep', testData);
  assertEquals(d.get('test.very.very.deep'), testData);
  d.set('test.very.very.deep', null);
  assert(!d.data.test.very.very.deep, 'Should be null');
});

Deno.test('Test deep set/get', () => {
  const d = new Db(true, test_db_file);
  assert(!d.data.test, 'Should be null');

  const testkey = 'test.deep.deep.very.deep';
  const testData = { test: 1 };
  d.set('test.deep.deep.very.deep', testData);

  assertEquals(d.data.test.deep.deep.very.deep, testData, 'get with props');
  assertEquals(d.get(testkey), testData, 'get with props');

  d.set(testkey, 3);
  assertEquals(d.data.test.deep.deep.very.deep, 3, 'get with props');
  assertEquals(d.get(testkey), 3, 'get with props');
});

Deno.test('Test deep get undefined', () => {
  const d = new Db(true, test_db_file);

  assert(d.data);
  assert(d.data.guilds);

  assert(!d.data.nope);
  assert(!d.get('nope'));
  assert(!d.get('nope.very.very.deep'));
});

Deno.test('Test an object deletion', () => {
  const d = new Db(true, test_db_file);

  const testData = { test: 1 };

  assert(d.data.authTokens && d.get('authTokens'));
  assert(!d.data.authTokens.anything && !d.get('authTokens.anything'));

  d.set('authTokens.anything', testData);
  assert(d.data.authTokens.anything && d.get('authTokens.anything'));
  assert(
    d.data.authTokens.anything.test && d.get('authTokens.anything.test') == 1,
  );
  d.set('authTokens.anything1', testData);
  assert(d.data.authTokens.anything1 && d.get('authTokens.anything1'));

  d.set('authTokens.anything', null);
  assert(!d.data.authTokens.anything && !d.get('authTokens.anything'));
  assert(d.data.authTokens.anything1 && d.get('authTokens.anything1'));

  assert(!d.data.nope);
  assert(!d.get('nope'));
  assert(!d.get('nope.very.very.deep'));
});

// const d = new Db(true, test_db_file);
// // console.log(d.data.config);
// d.set('recall', {recall:"test"})

// console.log("Access tests");
// console.log(d.data);
// console.log(d.data.config);
// console.log(d.data.config?.mode);
// console.log(d.data.guilds?.test);
