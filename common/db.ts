// deno-lint-ignore-file no-explicit-any
import { existsSync } from './deps.ts';

type db = Record<string, any>;
let cache = {};

const init = {
  guilds: {},
  authTokens: {},
};

// function size(s: any) {
//   return ~-encodeURI(s).split(/%..|./).length;
// }

export class Db {
  file: string | undefined;
  readonly key = 'postiedb';

  constructor(clear: boolean = false, file: string | undefined = undefined) {
    this.file = file != undefined ? file : Deno.env.get('DB_FILE');

    if (!this.file || this.file.trim() == '') {
      throw Error('Database file misconfigured');
    }

    // Reset
    if (clear) {
      this.update(init);
    }

    // Bare init
    if (existsSync(this.file)) {
      if (Object.entries(this.data).length === 0) {
        this.update(init);
      }
    } else {
      this.update(init);
    }
  }

  // Data Managment
  get data(): db {
    if (Object.entries(cache).length === 0) {
      try {
        cache = JSON.parse(Deno.readTextFileSync(this.file!));
      } catch (e) {
        console.log(this.file + ': ' + e.message);
      }
    }

    return cache;
  }
  update(newdata: any) {
    Deno.inspect(newdata);
    cache = JSON.parse(JSON.stringify(newdata));

    const val = JSON.stringify(cache);
    // console.log('size: ' + size(val));
    try {
      Deno.writeTextFileSync(this.file!, val);
    } catch (e) {
      console.log(this.file + ': ' + e.message);
    }
  }

  // Accessors

  get(key: string): any {
    const parts = key.split('.');
    let working = this.data;

    for (let i = 0; i < parts.length; i++) {
      if (!Object.hasOwn(working, parts[i])) {
        return undefined;
      }
      working = working[parts[i]];
    }
    return working;
  }

  set(key: any, val: any) {
    if (key.includes('undefined')) {
      throw new Error('Should not use undefined as key in: ', key);
    }

    const orig = this.data;
    let working = orig;

    let i;
    key = key.split('.');
    for (i = 0; i < key.length - 1; i++) {
      if (!Object.hasOwn(working, key[i])) {
        working[key[i]] = {};
      }
      working = working[key[i]];
    }

    if (val == null) {
      delete working[key[i]];
    } else {
      working[key[i]] = val;
    }

    this.update(orig);
    return;
  }
}
