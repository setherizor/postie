import { Postie } from './src/bot.ts';

// Setup instance of postie
const client = new Postie({
  token: Deno.env.get('DISCORD_BOT_TOKEN') as string,
  owners: [Deno.env.get('OWNER_USER_ID') as string],
  prefix: Deno.env.get('COMMAND_PREFIX') ?? '-',
});

// Connect, and hand out access to bot
export const Bot = await (async () => {
  const ping = client.gateway?.ping;

  if (!ping || ping == 0) {
    return await client.connect();
  } else if (ping && ping > 20000) {
    console.log('Reconnecting due to high ping: ' + ping);
    return await client.reconnect();
  }

  return client;
})();
