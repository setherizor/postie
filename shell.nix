{ pkgs ? import <nixpkgs> { } }:
with pkgs;
mkShell rec {
  packages = [ deno ];

  shellHook = ''
    export PATH="$PATH:~/.deno/bin/"
    deno install -qAn vr https://deno.land/x/velociraptor@1.5.0/cli.ts
    echo -e "\nLoaded Postie Environment"
  '';
}
