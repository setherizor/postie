# Postie Bot

A convience bot for my friends and I.

## Built with

- Deno
- Harmony
  - Music features based on [MusicBox](https://github.com/DjDeveloperr/MusicBox)
- [Lavalink](https://github.com/freyacodes/Lavalink)

## Features

- [x] Installable Slash Commands
- [x] Reaction Roles
  - [x] With after-the-fact editing
- [x] Message Cleaning
- [x] Audio Search, Playback, & Queues
- [x] Basic Configurable Soundboard
- [x] Persistant DB (Deno Webstorage)
- [x] Web Dashboard
  - [x] Authentication flow
  - [x] Permissions
  - [x] Guild Settings Managment
  - [x] Oauth2 Tokens and Regeneration
  - [x] Custom Role-Based Permissions

## Roadmap

- [ ] Recurring Discord Events
- [ ] Meme/Image Recall (low priority)

## Inspirations

- https://github.com/reboxer/discord-oauth2/blob/ffbc59a48afa5941738ad8e0eadf985cb1e4989d/lib/oauth.js

<!-- TODO: Write health checker for the bot and for lavalink -->

<!-- TURN INTO /e commands -->

<!--
# Show top level DB
//e c.client.db.data

# Clear top level DB
//e c.client.db.set('', c.client.db.defaults)
 -->
