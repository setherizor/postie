export * from 'https://deno.land/x/harmony@v2.9.1/mod.ts';
export type { Track } from 'https://deno.land/x/lavalink_types@2.0.6/mod.ts';
export * as lava from 'https://raw.githubusercontent.com/lavaclient/lavadeno/22c88554cfc6f06dee7838eca0b00960cf47477a/mod.ts';
export { Lavalink } from 'https://raw.githubusercontent.com/lavaclient/lavadeno/22c88554cfc6f06dee7838eca0b00960cf47477a/deps.ts';
export { existsSync } from './common/deps.ts';
export { isBotMaster } from './src/util/isbotmaster.ts';

export function scale(
  number: number,
  inMin = 0,
  inMax = 100,
  outMin = 0,
  outMax = 60,
) {
  return (number - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}
