import { Context, Db, Group, HandlerFunc, HttpException, log } from './deps.ts';
import { Bot, ensureLoggedIn, Guild } from './deps.ts';
import { API, discordProxy, getGuilds } from './utils.ts';

export default function (app: Group) {
  const botDb = new Db();

  // ==============================
  // ===== Data Get Handlers ======
  // ==============================

  app.get('/health', (_c: Context) => {
    if (Bot.uptime != 0 && Bot.gateway?.connected && Bot.gateway?.ping < 2000) {
      return true;
    } else {
      log(
        'HTTP',
        'healthcheck encountered an error',
      );
      throw new HttpException('Health issue', 500);
    }
  });

  // https://discord.com/developers/docs/topics/oauth2#get-current-authorization-information
  app.get('/user', async (c: Context) => {
    const token = c.get('access_token') as string;

    return await discordProxy(' ', `${API}/users/@me`, {
      headers: {
        'Authorization': 'Bearer ' + token,
      },
    });
  }, ensureLoggedIn);

  app.get('/guilds', async (c: Context) => {
    const token = c.get('access_token') as string;

    const botguilds = await Bot.guilds.keys();
    const userguilds = await getGuilds(token);

    const sharedguilds = [...userguilds.values()].filter((g: Guild) =>
      botguilds.includes(g.id)
    ).sort(
      // deno-lint-ignore no-explicit-any
      (a: { name: number; owner: any }, b: { name: number; owner: any }) => {
        if (a.name < b.name || (!(a.owner) && (b.owner))) {
          return -1;
        }
        if (a.name > b.name || ((a.owner) && !(b.owner))) {
          return 1;
        }
        return 0;
      },
    );

    return sharedguilds;
  }, ensureLoggedIn);

  app.get('/roles/:guildId', async (c: Context) => {
    const token = c.get('access_token') as string;
    const { guildId } = c.params;
    return await discordProxy(token, `${API}/guilds/${guildId}/roles`);
  }, ensureLoggedIn);

  app.get('/channels/:guildId', async (c: Context) => {
    const token = c.get('access_token') as string;
    const { guildId } = c.params;
    return (await discordProxy(
      token,
      `${API}/guilds/${guildId}/channels`,
    ) as Record<string, unknown>[]).filter((c) => c.type != 4);
  }, ensureLoggedIn);

  app.get('/emojis/:guildId', async (c: Context) => {
    const token = c.get('access_token') as string;
    const { guildId } = c.params;
    return await discordProxy(token, `${API}/guilds/${guildId}/emojis`);
  }, ensureLoggedIn);

  app.get('/botmasters/:guildId', (c: Context) => {
    const { guildId } = c.params;
    return botDb.get(`guilds.${guildId}.botMasters`) ?? [];
  }, ensureLoggedIn);

  // ==============================
  // ===== Data Post Handlers =====
  // ==============================

  // Check that people are authed to perform whatever they are doing
  app.app.use((next: HandlerFunc) => async (c: Context) => {
    // deno-lint-ignore no-explicit-any
    const body = await c.body as Record<string, any>;
    const token = c.get('access_token') as string;

    if (c.method == 'POST' && !token) {
      log('HTTP', 'post rejected due to not logged in');
      return c.json({ type: 'error', message: 'not logged in' }, 401);
    } else if (body.guild) {
      const userguilds = await getGuilds(token);
      if (!userguilds.find((g) => g.id == body.guild).canManage) {
        log('HTTP', 'post rejected due to not being admin on guild');
        return c.json({
          type: 'error',
          message: 'not permitted to interact with guild',
        }, 401);
      }
    }
    return next(c);
  });

  app.post('/botmasters', async (c: Context) => {
    const { guild, mode, role } = await c.body as {
      guild: string;
      mode: string;
      role: string;
    };

    if (c.request.headers.get('HX-Request')) {
      c.response.headers.set('HX-Trigger', 'getmasters');
    }

    try {
      const roleData = JSON.parse(atob(role));
      const roleId = roleData.id;

      if (guild) {
        let masters = botDb.get(`guilds.${guild}.botMasters`);
        masters = masters ? masters : {};
        if (mode == 'add') {
          masters[roleId] = role;
          botDb.set(`guilds.${guild}.botMasters.${roleId}`, roleData);
        } else if (mode == 'remove') {
          botDb.set(`guilds.${guild}.botMasters.${roleId}`, null);
        }
        return true;
      }
    } catch (error) {
      return error;
    }
  }, ensureLoggedIn);

  app.post('/createReactionMessage', async (c: Context) => {
    const token = c.get('access_token') as string;

    if (!token) {
      return c.json({ type: 'error', message: 'not logged in' }, 401);
    }

    try {
      const { guild, channel, message, reactionRoles } = await c.body as {
        guild: string;
        channel: string;
        message: string;
        // deno-lint-ignore no-explicit-any
        reactionRoles: Record<string, any>[];
      };

      if (!guild) {
        return c.json({ type: 'error', message: 'no guild selected' }, 400);
      }

      const roleMessage = await discordProxy(
        token,
        `${API}/channels/${channel}/messages`,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            content: message,
            allowed_mentions: {
              parse: [],
            },
          }),
        },
      );

      if (roleMessage.id == undefined || roleMessage.id == 'undefined') {
        log(JSON.stringify(roleMessage));
        throw new Error('rolemessage undefined');
      }

      Object.keys(reactionRoles).forEach(async (r) =>
        await discordProxy(
          token,
          `${API}/channels/${channel}/messages/${roleMessage.id}/reactions/${
            atob(r)
          }/@me`,
          {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
            },
          },
        )
      );

      botDb.set(
        `guilds.${guild}.reactionMessages.${channel}@${roleMessage.id}`,
        reactionRoles,
      );

      return 'success';
    } catch (error) {
      console.log(error);
      return 'error' + error;
    }
  }, ensureLoggedIn);
}
