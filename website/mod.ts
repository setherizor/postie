import { Application, log, OAuth2Client } from './deps.ts';
import { pug } from './utils.ts';

const oauth2Client = new OAuth2Client({
  clientId: Deno.env.get('OAUTH2_CLIENT_ID') as string,
  clientSecret: Deno.env.get('OAUTH2_CLIENT_SECRET') as string,
  authorizationEndpointUri: 'https://discord.com/api/oauth2/authorize',
  tokenUri: 'https://discord.com/api/oauth2/token',
  redirectUri: Deno.env.get('OAUTH2_CALLBACK') as string,
});

import { Tasks } from './tasks.ts';
Tasks(oauth2Client);

const app = new Application();
const port = 8080;

import authroutes from './authroutes.ts';
authroutes(app.group('auth'), oauth2Client);

import api from './api.ts';
api(app.group('api'));

import fragments from './fragments.ts';
fragments(app.group('x'));

app
  .static('/', 'public')
  .get('/', (c) => pug(c, 'public/index.pug', {}) as string)
  .start({ port });

log('Website', 'Postie\'s Website running on http://localhost:' + port);
