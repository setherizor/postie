import { Context, Db, Group, HandlerFunc, HttpException, log } from './deps.ts';
import type { OAuth2Client } from './deps.ts';

function clearCookies(c: Context) {
  c.setCookie({
    name: 'authstate',
    value: '',
    expires: new Date(0),
  });
  c.setCookie({
    name: 'isLoggedIn',
    value: '',
    expires: new Date(0),
  });
}

export const ensureLoggedIn = (next: HandlerFunc) => async (c: Context) => {
  if (c.cookies?.isLoggedIn == 'true') {
    return await next(c);
  }

  if (c.request.headers.get('HX-Request')) {
    c.response.headers.set('HX-Redirect', '/auth/login');
  }

  c.response.status = 401;
  return new HttpException('Not Authenticated', 401);
};

export default function (app: Group, oauth2Client: OAuth2Client) {
  const botDb = new Db();

  // ==============================
  // ======= Authentication =======
  // ==============================

  // Register a global handler
  app.app.use((next: HandlerFunc) => async (c: Context) => {
    if (['/ucp', '/inform'].some((x) => c.path.includes(x))) {
      return {};
    }

    // Extract params
    let { state, code } = c.queryParams as unknown as {
      state: string;
      code: string;
    };
    // If we are coming back from an actual authentication vs a bot invite
    if (c.path == '/' && Boolean(state) && Boolean(code)) {
      try {
        // Exchange the code for the user's access token
        const tokens = await oauth2Client.code.getToken(c.url);

        const existingToken = Object.entries(botDb.data.authTokens).find(
          (x) =>
            (x[1] as Record<string, unknown>).accessToken ==
              tokens.accessToken,
        )?.[0];
        if (existingToken) {
          state = existingToken;
        }

        botDb.set(`authTokens.${state}`, tokens);
        botDb.set(`authTokens.${state}.timestamp`, Date.now());

        log('HTTP', 'exchanged for & stored the auth token also');

        c.setCookie({
          name: 'authstate',
          value: state,
          maxAge: tokens.expiresIn! * 1000,
          httpOnly: true,
        });

        c.setCookie({
          name: 'isLoggedIn',
          value: 'true',
          maxAge: tokens.expiresIn! * 1000,
          httpOnly: false,
        });
        log('HTTP', 'set user cookie');
      } catch (error) {
        log(
          'HTTP',
          'requesting access_token from discord went wrong: ',
          error,
        );
      }

      return c.redirect('/');
    }

    // Setup & Manage cookies for other middlewares
    const { authstate } = c.cookies;
    if (authstate) {
      // Retrieve access_token from DB and attach to context
      const token = botDb.get(`authTokens.${authstate}.accessToken`);
      if (!token) {
        log('HTTP', 'clearing cookies for session', authstate);
        clearCookies(c);
      }
      c.set('access_token', token);
    }

    return next(c);
  });

  // Oauth Urls
  app.get(
    '/invite',
    (c: Context) =>
      c.redirect(
        oauth2Client.code.getAuthorizationUri({
          scope: 'bot applications.commands',
        }).toString() + '&permissions=' + 1099511099383, // https://discordapi.com/permissions.html#1099511099383
      ),
  );

  app.get(
    '/login',
    (c: Context) =>
      c.redirect(
        oauth2Client.code.getAuthorizationUri({
          scope: 'identify email guilds',
          state: Array.from(crypto.getRandomValues(new Uint8Array(16))).map((
            x,
          ) => x.toString(16)).join(''),
        }).toString(),
      ),
  );

  app.get(
    '/logout',
    async (c: Context) => {
      const token = c.get('access_token') as string;
      const { authstate } = c.cookies;

      log('HTTP', 'cleared user auth data from db & cookies');
      clearCookies(c);
      botDb.set(`authTokens.${authstate}`, null);

      const revoke = fetch(
        'https://discord.com/api/oauth2/token/revoke',
        {
          method: 'POST',
          body: new URLSearchParams({
            client_id: Deno.env.get('OAUTH2_CLIENT_ID') as string,
            client_secret: Deno.env.get('OAUTH2_CLIENT_SECRET') as string,
            token,
          }),
        },
      );

      log('HTTP', 'revoked user token, res:', await (await revoke).json());

      log('HTTP', 'logged user out, id:', authstate);
      return c.redirect('/');
    },
    ensureLoggedIn,
  );
}
