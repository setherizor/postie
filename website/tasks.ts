import { Db, log, OAuth2Client } from './deps.ts';

const bufferMiliseconds = 7 * 1000 * 60 * 60 * 24; // one day in seconds

function checkTokenExpiry(oauth: OAuth2Client, botDb: Db) {
  log('Task', 'running token expiry check');
  // find tokens expiring with the next day and a half
  const authStates = Object.keys(botDb.data.authTokens);
  // Get keys for tokens soon to expire
  const expiringTokenKeys = authStates.filter((s) => {
    const o = botDb.data.authTokens[s];
    return (
      o.timestamp + (o.expiresIn * 1000) < Date.now() + bufferMiliseconds * 1.5 // is expiry less than a day and a half from now
    );
  });

  // Regenerate them
  expiringTokenKeys.forEach(async (key) => {
    log('Regenerating expiring token: ' + key);

    const refreshToken = botDb.get(`authTokens.${key}.refreshToken`);

    try {
      botDb.set(
        `authTokens.${key}`,
        await oauth.refreshToken.refresh(refreshToken, {
          requestOptions: {
            headers: {
              Authorization: '',
            },
            body: {
              client_id: Deno.env.get('OAUTH2_CLIENT_ID') as string,
              client_secret: Deno.env.get('OAUTH2_CLIENT_SECRET') as string,
            },
          },
        }),
      );

      botDb.set(`authTokens.${key}.timestamp`, Date.now());
    } catch (error) {
      log('Token regen failed ' + error);
      const tmp = botDb.get('authTokens');
      delete tmp[key];
      botDb.set('authTokens', tmp);
      log('Deleted token ' + key);
    }
  });
}

// Handler for expiring tokens
export const Tasks = (oAuth2Client: OAuth2Client) => {
  // Run checks now and every day
  const db = new Db();

  function runTask() {
    try {
      checkTokenExpiry(oAuth2Client, db);
    } catch (e) {
      log('Tasks', e);
    }
    setTimeout(runTask, bufferMiliseconds / 2);
  }

  runTask();
};
