import { Collection, Context } from './deps.ts';
import { renderFile } from 'https://cdn.jsdelivr.net/gh/lumeland/pug@master/mod.ts';

// deno-lint-ignore no-explicit-any
export function pug(c: Context, file: string, opts: any) {
  const defaults = {
    doctype: 'html',
    cache: false,
    isHTMX: c.request.headers.has('HX-Request'),
  };
  return renderFile(file, Object.assign(defaults, opts)) as string;
}

import { log } from './deps.ts';

export const API = 'https://discord.com/api/v9';
// Used for custom param deciding if the user should be allowed to interact with a guild
// https://discord.com/developers/docs/topics/permissions#permissions-bitwise-permission-flags
export const isServerAdmin = (x: number) => (x & (1 << 3)) == 1 << 3;
// deno-lint-ignore no-explicit-any
export const canManage = (g: any) =>
  isServerAdmin(g.permissions) ||
  g.id == (Deno.env.get('OWNER_USER_ID') as string)!;

export const discordProxy = async (
  token: string,
  url: string,
  opts: Record<string, unknown> | null = null,
) => {
  if (!token) {
    return {};
  }

  const sleep = (ms: number) =>
    new Promise((resolve) => setTimeout(resolve, ms));

  const defaults = {
    headers: {
      'Authorization': 'Bot ' +
        Deno.env.get('DISCORD_BOT_TOKEN') as string,
    },
  };

  if (opts?.headers) {
    opts.headers = Object.assign(defaults.headers, opts.headers);
  }

  let res;

  try {
    res = await fetch(url, opts ?? defaults);
    let json = await res.json();
    // Rate Limits...
    while (!json?.global && json?.message?.includes('rate limited')) {
      await sleep(json.retry_after * 1000 + 10);
      res = await fetch(url, opts ?? defaults);
      json = await res.json();
    }
    return json;
  } catch (error) {
    if (res && res.status == 204) {
      return 'success';
    } else {
      log(error);
      return 'error';
    }
  }
};

export const getGuilds = async (token: string) => {
  // deno-lint-ignore no-explicit-any
  const guilds: Collection<any> = await discordProxy(
    token,
    `${API}/users/@me/guilds`,
    {
      headers: {
        'Authorization': 'Bearer ' + token,
      },
    },
  );

  // deno-lint-ignore no-explicit-any
  guilds.forEach((g: any) => {
    g.canManage = canManage(g);
  });

  return guilds;
};
