import { Postie } from '../deps.ts';
import { Context, Db, Group } from './deps.ts';
import { Bot, ensureLoggedIn, Guild } from './deps.ts';
import { API, discordProxy, getGuilds, pug } from './utils.ts';

export default function (app: Group) {
  const botDb = new Db();

  // ==============================
  // === View Fragment Handlers ===
  // ==============================

  app.get('/pickGuild', async (c: Context) => {
    const token = c.get('access_token') as string;
    const botguilds = await Bot.guilds.keys();
    const userguilds = await getGuilds(token);

    const sharedguilds = [...userguilds.values()].filter((g: Guild) =>
      botguilds.includes(g.id)
    ).sort(
      // deno-lint-ignore no-explicit-any
      (a: { name: number; owner: any }, b: { name: number; owner: any }) => {
        if (a.name < b.name || (!(a.owner) && (b.owner))) {
          return -1;
        }
        if (a.name > b.name || ((a.owner) && !(b.owner))) {
          return 1;
        }
        return 0;
      },
    );

    return pug(c, 'public/fragments/pickGuild.pug', {
      guilds: sharedguilds,
    }) as string;
  }, ensureLoggedIn);

  app.get('/botmasters/:guildId', (c: Context) => {
    const { guildId } = c.params;

    const botmasters = botDb.get(`guilds.${guildId}.botMasters`) ?? [];

    return pug(c, 'public/fragments/botmasters.pug', {
      botmasters,
    }) as string;
  }, ensureLoggedIn);

  app.get('/manageGuild/:guildId', async (c: Context) => {
    const { guildId } = c.params;
    const token = c.get('access_token') as string;

    const guild = await Bot.guilds.get(guildId);

    const channels = (await discordProxy(
      token,
      `${API}/guilds/${guildId}/channels`,
    ) as Record<string, unknown>[]).filter((c) => c.type != 4);

    const roles = await discordProxy(token, `${API}/guilds/${guildId}/roles`) ??
      [];

    // check and prune reactions on fetch
    await (Bot as Postie).setupReactionMessage(true);

    const reactionMessagesSource =
      botDb.get(`guilds.${guildId}.reactionMessages`) ?? [];

    const reactionMessages = await Promise.all(
      Object.keys(reactionMessagesSource).map(
        async (x) => {
          const [channelId, messageId] = x.split('@');
          const channel = await Bot.channels.get(channelId);
          const message = await channel?.messages.get(messageId);

          const roles = await Promise.all(
            Object.values(reactionMessagesSource[x]).map(async (rId) =>
              await guild?.roles.get(rId)
            ),
          );

          return {
            guildId: channel?.guild?.id,
            channelId,
            channelName: channel?.name,
            messageId,
            messageAuthor: message.author.username,
            messageContent: message.content,
            roles: roles.map((x) => ({
              id: x?.id,
              name: x?.name,
              color: x?.color,
            })),
          };
        },
      ),
    );

    return pug(c, 'public/fragments/manageGuild.pug', {
      guild,
      channels,
      roles,
      reactionMessages,
    }) as string;
  }, ensureLoggedIn);
}
