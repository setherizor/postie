import {
  ApplicationCommandPartial,
  ApplicationCommandType,
} from '../../../deps.ts';

export const commands: ApplicationCommandPartial[] = [
  {
    name: 'Edit Reaction Roles',
    type: ApplicationCommandType.MESSAGE,
  },
  {
    name: 'soundboard',
    description: 'Update effects and generate a new soundboard!',
  },
];
