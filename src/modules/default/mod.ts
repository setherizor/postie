import {
  ApplicationCommandInteraction,
  ButtonStyle,
  Client,
  customValidation,
  GuildTextChannel,
  InteractionResponseType,
  isBotMaster,
  Member,
  Message,
  MessageComponentData,
  MessageComponentInteraction,
  MessageComponentType,
  messageContextMenu,
  MessageReaction,
  Postie,
  Role,
  RolePayload,
  slash,
  SlashModule,
  User,
} from '../../../deps.ts';

import { commands } from './commands.ts';
import { log } from '../../util/log.ts';
import { Db } from '../../../common/db.ts';

const db = new Db();

export class DefaultModule extends SlashModule {
  name = 'Default';
  client: Client;
  definitions = commands;
  // Used to help with the "async-ie-ness" for editing reaction roles
  // deno-lint-ignore no-explicit-any
  editCache: Record<string, Record<string, any>>;
  sbshortcuts?: Record<string, string>;

  constructor(client: Client) {
    super();
    this.client = client;
    this.editCache = {};
  }

  #clearRRCache(guildID: string) {
    Object.keys(this.editCache[guildID]).forEach((x: string) =>
      this.editCache[guildID][x] = undefined
    );

    delete this.editCache[guildID];
  }

  /**
   * Edit Reaction Roles Explanation
   * editReactionRoles
   *   receives message context menu event, prompts for role select with component
   *
   * editReactionRolesInteractionHandler
   *   receives role select component choice(s) event, defers response to interaction
   *
   * editReactionRolesReactionHandler
   *   receives message reaction event, only functions if the previous was just ran (checks cache)
   *   saves reaction role to db and replies with confirmation
   */

  @messageContextMenu('Edit Reaction Roles')
  @customValidation(isBotMaster, 'you do not have permissions for this')
  async editReactionRoles(d: ApplicationCommandInteraction) {
    const gid = d.guild!.id;
    this.editCache[gid] = {};
    this.editCache[gid]._initiator = d.member;
    this.editCache[gid].channel = d.channel;
    const target = Object.values(d.resolved.messages)[0];
    this.editCache[gid].message = target;

    await d.respond({
      content:
        `You selected \`${target.author.username} : ${target.content}\` to add a reaction role to.\n\n:one: select a role\n:two: react to the target message`,
      components: [
        {
          type: MessageComponentType.ActionRow,
          components: [{
            type: 6,
            // @ts-expect-error TODO:not in harmony api
            custom_id: 'editrr::role_select',
            placeholder: 'Select a role to add?',
            min_values: 1,
            max_values: 1,
          }],
        },
      ],
      ephemeral: true,
    });
  }

  // Throws back from the catchall interaction handler in bot.ts based on editrr:: id prefix
  async editReactionRolesInteractionHandler(d: MessageComponentInteraction) {
    const gid = d.guild!.id;
    if (!this.editCache[gid]) {
      return;
    }

    this.editCache[gid]._interaction = d;
    const i = d as unknown as ApplicationCommandInteraction;
    const role = (Object.values(
      i.data.resolved?.roles as Record<string, unknown>,
    ) as RolePayload[])[0];

    if (!role || !role.id) {
      log('Role not found from interaction?');
      return;
    }

    this.editCache[gid].role = role;

    await d.respond({
      type: InteractionResponseType.DEFERRED_MESSAGE_UPDATE,
    });
  }

  // Throws back from the catchall interaction handler in bot.ts based on reaction checking "cache for enabling logic"
  async editReactionRolesReactionHandler(
    reaction: MessageReaction,
    reactor: User,
  ): Promise<boolean> {
    const gid = reaction.message.guildID!;
    // Check if we even have a cache, and are thus running a reaction role edit
    if (!this.editCache[gid]) {
      return false;
    }

    // Load in vars from the cache
    const role = this.editCache[gid].role as Role;
    const member = this.editCache[gid]._initiator as Member;
    const target = this.editCache[gid].message as Message;
    const d = this.editCache[gid]._interaction as MessageComponentInteraction;
    this.editCache[gid].reaction = reaction;

    // Make sure we are dealing with the same user & guild
    const guildID = (target.channel as GuildTextChannel).guildID;
    const sameUser = member && member.id == reactor.id;
    const sameGuild = guildID && guildID == d.guild?.id;
    if (!sameUser || !sameGuild) {
      log('Exit editrr because of inequality');
      return false;
    }

    if (Object.values(this.editCache).some((x) => x == undefined)) {
      await d.editResponse({
        content: `Aborted due to the edit RR cache not fully defined`,
      });
      return true;
    }

    // Now we have all the parts and can update the DB
    try {
      const prepReact = reaction.emoji.name +
        (reaction.emoji.id ? ':' + reaction.emoji.id : '');
      const encodedReaction = btoa(encodeURIComponent(prepReact));

      log(
        `Adding reaction role to message:\n${encodedReaction} ====  ${role.id}`,
      );

      // Put record in the database
      const access =
        `guilds.${guildID}.reactionMessages.${target.channelID}@${target.id}`;
      target.addReaction(reaction.emoji.name!);
      db.set(access + `.${encodedReaction}`, role.id);

      const url =
        `https://discord.com/channels/${guildID}/${target.channelID}/${target.id}`;
      await d.editResponse({
        content:
          `:white_check_mark: Sucessfully added \`${role.name}\` as a reaction role.\nEverything seems to have went through!`,
        embeds: [
          {
            description: '[Go To Message](' + url + ')',
            url,
          },
        ],
        ephemeral: true,
      });

      this.#clearRRCache(gid);
      log('Finished editing reaction role & cleared cache');
    } catch (e) {
      await d.editResponse({
        content: `Something went awry\n\`\`\`js\n${Deno.inspect(e)}\n\`\`\``,
      });
    }

    // True skips rest of original handler
    return (this.editCache[gid]?._initiator == undefined);
  }

  @slash()
  @customValidation(isBotMaster, 'you do not have permissions for this')
  //Only allow this command if the bot is in a voice channel
  async soundboard(d: ApplicationCommandInteraction) {
    // update the soundboard config
    // deno-lint-ignore no-explicit-any
    (this.client as any).sbshortcuts =
      // deno-lint-ignore no-explicit-any
      (this.client as any).db.get(`soundboard`) ?? {};

    // deno-lint-ignore no-explicit-any
    const btns = Object.keys((this.client as any).sbshortcuts);

    const components: MessageComponentData[] = [];

    let x = 0;
    for (let i = 0; i < Math.ceil(btns.length / 5); i++) {
      const things: MessageComponentData[] = [];

      for (let j = 0; j < 5; j++) {
        const b = btns[x++];

        if (b == undefined) {
          continue;
        }

        things.push({
          type: MessageComponentType.Button,
          style: ButtonStyle.BLURPLE,
          // emoji: { name: '🔊' },
          label: b,
          customID: 'sb::' + b,
        });
      }

      components.push(
        {
          type: MessageComponentType.ActionRow,
          components: things,
        },
      );
    }

    components.push(
      {
        type: MessageComponentType.ActionRow,
        components: [
          {
            type: MessageComponentType.Button,
            style: ButtonStyle.GREEN,
            emoji: { name: '🔊' },
            label: 'Play',
            customID: 'media::Play',
          },
          {
            type: MessageComponentType.Button,
            style: ButtonStyle.GREY,
            emoji: { name: '🔈' },
            label: 'Pause',
            customID: 'media::Pause',
          },
          {
            type: MessageComponentType.Button,
            style: ButtonStyle.RED,
            emoji: { name: '🔇' },
            label: 'Stop',
            customID: 'media::Stop',
          },
          {
            type: MessageComponentType.Button,
            style: ButtonStyle.SECONDARY,
            emoji: { name: '🤔' },
            label: 'Random',
            customID: 'media::Random',
          },
        ],
      },
    );

    await d.respond({
      content: 'Let\'s get it playing 🎧',
      components,
    });
  }

  // Throws back from the catchall interaction handler in bot.ts based on editrr:: id prefix
  async soundboardInteractionHandler(
    d: MessageComponentInteraction,
    c: Postie,
  ) {
    // If unloaded, get config from online resource
    const key = d.data.custom_id.split('::').pop()!;

    if (d.customID.startsWith('sb::')) {
      if (!c.sbshortcuts) {
        log('Soundboard', 'fetching soundboard configs');
        c.sbshortcuts = c.db.get(`soundboard`) ?? {};
      }

      // deno-lint-ignore no-prototype-builtins
      if (c.sbshortcuts!.hasOwnProperty(key)) {
        await c.modules.music.playByte(d, c.sbshortcuts![key]);
      } else {
        await d.reply({
          content:
            `could not find soundbyte: \`${key}\`, try running \`/soundboard\` again`,
          ephemeral: true,
        });
      }
    } else if (d.customID.startsWith('media::')) {
      switch (d.data.custom_id.split('::').pop()) {
        case 'Play': {
          c.modules.music.resume(
            d as unknown as ApplicationCommandInteraction,
          );
          break;
        }
        case 'Pause':
          c.modules.music.pause(
            d as unknown as ApplicationCommandInteraction,
          );
          break;
        case 'Stop':
          c.modules.music.leave(
            d as unknown as ApplicationCommandInteraction,
          );
          break;
        case 'Random': {
          if (!c.sbshortcuts) {
            log('Soundboard', 'fetching soundboard configs');
            c.sbshortcuts = c.db.get(`soundboard`) ?? {};
          }
          const s = Object.keys(c.sbshortcuts!);
          await c.modules.music.playByte(
            d,
            c
              .sbshortcuts![
                s[Math.floor(Math.random() * Object.keys(s).length)]
              ],
          );
          break;
        }
      }
    }
  }
}
