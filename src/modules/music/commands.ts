import {
  ApplicationCommandOptionType,
  ApplicationCommandPartial,
} from '../../../deps.ts';

export const commands: ApplicationCommandPartial[] = [
  {
    name: 'music',
    description: 'Interact with the music and media that I can provide',
    options: [
      {
        name: 'play',
        description: 'Start playing music in your current VC!',
        type: ApplicationCommandOptionType.SUB_COMMAND,
        options: [
          {
            name: 'query',
            description: 'Query to search for music on YouTube.',
            type: ApplicationCommandOptionType.STRING,
            required: true,
          },
        ],
      },
      {
        name: 'search',
        description: 'Search for music on YouTube!',
        type: ApplicationCommandOptionType.SUB_COMMAND,
        options: [
          {
            name: 'query',
            description: 'Query to search for music on YouTube.',
            type: ApplicationCommandOptionType.STRING,
            required: true,
          },
        ],
      },
      {
        name: 'pause',
        description: 'Pause the music.',
        type: ApplicationCommandOptionType.SUB_COMMAND,
        options: [],
      },
      {
        name: 'resume',
        description: 'Resume the paused music.',
        type: ApplicationCommandOptionType.SUB_COMMAND,
        options: [],
      },
      {
        name: 'skip',
        description: 'Skip current track.',
        type: ApplicationCommandOptionType.SUB_COMMAND,
        options: [],
      },
      {
        name: 'join',
        description: 'Make the bot join your current VC.',
        type: ApplicationCommandOptionType.SUB_COMMAND,
        options: [],
      },
      {
        name: 'leave',
        description: 'Make the bot leave your current VC.',
        type: ApplicationCommandOptionType.SUB_COMMAND,
        options: [],
      },
      {
        name: 'nowplaying',
        description: 'Check info of the current track being played.',
        type: ApplicationCommandOptionType.SUB_COMMAND,
        options: [],
      },
      {
        name: 'replay',
        description: 'Replay the current track from start.',
        type: ApplicationCommandOptionType.SUB_COMMAND,
      },
      {
        name: 'volume',
        description: 'View or set volume of the player.',
        type: ApplicationCommandOptionType.SUB_COMMAND,
        options: [
          {
            name: 'new',
            description: 'New volume to set if any.',
            required: false,
            type: ApplicationCommandOptionType.INTEGER,
          },
        ],
      },
      {
        name: 'seek',
        description: 'Change player\'s track position.',
        type: ApplicationCommandOptionType.SUB_COMMAND,
        options: [
          {
            name: 'pos',
            description: 'New Track Position, such as 0:30.',
            type: ApplicationCommandOptionType.STRING,
            required: true,
          },
        ],
      },
      {
        name: 'forward',
        description: 'Forward player\'s track position.',
        type: ApplicationCommandOptionType.SUB_COMMAND,
        options: [
          {
            name: 'pos',
            description: 'Time to forward track position, such as 0:10.',
            type: ApplicationCommandOptionType.STRING,
            required: true,
          },
        ],
      },
      {
        name: 'rewind',
        description: 'Rewind player\'s track position.',
        type: ApplicationCommandOptionType.SUB_COMMAND,
        options: [
          {
            name: 'pos',
            description: 'Time to rewind Track Position, such as 0:10.',
            type: ApplicationCommandOptionType.STRING,
            required: true,
          },
        ],
      },
      {
        name: 'queue',
        description: 'Modify the music queue',
        type: ApplicationCommandOptionType.SUB_COMMAND_GROUP,
        options: [
          {
            name: 'show',
            description: 'Check current tracks queue.',
            type: ApplicationCommandOptionType.SUB_COMMAND,
            options: [
              {
                name: 'page',
                type: ApplicationCommandOptionType.INTEGER,
                description: 'Optional page number to look for in queue.',
                required: false,
              },
            ],
          },
          {
            name: 'loopqueue',
            description: 'Enable disable looping Tracks Queue.',
            type: ApplicationCommandOptionType.SUB_COMMAND,
            options: [],
          },
          {
            name: 'remove',
            description: 'Remove a track from queue.',
            type: ApplicationCommandOptionType.SUB_COMMAND,
            options: [
              {
                name: 'position',
                type: ApplicationCommandOptionType.INTEGER,
                description: 'Position of track in the queue.',
                required: true,
              },
            ],
          },
          {
            name: 'movetrack',
            description: 'Swap a track to top or swap positions of two tracks.',
            type: ApplicationCommandOptionType.SUB_COMMAND,
            options: [
              {
                name: 'track',
                type: ApplicationCommandOptionType.INTEGER,
                description: 'Position of track to move.',
                required: true,
              },
              {
                name: 'track2',
                type: ApplicationCommandOptionType.INTEGER,
                description:
                  'Position of second track if you want to swap two.',
                required: false,
              },
            ],
          },
        ],
      },
    ],
  },
];
