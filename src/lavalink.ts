// deno-lint-ignore-file no-explicit-any
import {
  Client,
  Collection,
  Guild,
  lava,
  scale,
  Track,
  User,
} from '../deps.ts';
import type { MusicSlashModule } from './modules/music/mod.ts';
import { log } from './util/log.ts';

let envperm = false;
try {
  Deno.env.get('NOTHING');
  envperm = true;
} catch (_e) {
  envperm = false;
}

const config: any = {};
if (envperm == true) {
  config.lavalink = {};
  config.lavalink.host = Deno.env.get('LAVALINK_HOST');
  config.lavalink.port = Deno.env.get('LAVALINK_PORT');
  config.lavalink.password = Deno.env.get('LAVALINK_PASSWORD');
  config.mainGuild = Deno.env.get('MAINGUILD');
}

if (
  !config.lavalink.host ||
  !config.lavalink.port ||
  !config.lavalink.password ||
  !config.mainGuild
) {
  throw new Error(
    'Failed to retreive Lavalink credentials. Add ENV vars.',
  );
}

export const nodes = [
  {
    id: 'master',
    host: config.lavalink.host,
    port: config.lavalink.port,
    password: config.lavalink.password,
  },
];

export const createManager = (client: Client) => {
  const mg = new lava.Cluster({
    nodes,
    sendGatewayPayload(id, payload) {
      if (client.shards.list.size != 1) {
        log('Bot', `there are ${client.shards.list.size} gateways!`);
      }
      const shard = client.shards.get(
        Number((id << 22n) % BigInt(client.shards.list.size)),
      )!;
      shard.send(payload);
    },
  });

  client.on('raw', (evt: string, d: any) => {
    if (evt === 'VOICE_SERVER_UPDATE') mg.handleVoiceUpdate(d);
    else if (evt === 'VOICE_STATE_UPDATE') mg.handleVoiceUpdate(d);
  });

  return mg;
};

export const formatLength = (len: number) => {
  len = Math.floor(len / 1000);
  const secs = len % 60;
  const mins = (len - secs) / 60;
  return `${mins}:${secs < 10 ? '0' : ''}${secs}`;
};

export const parseLength = (pos: string): undefined | number => {
  const spl = pos.split(':').map((e) => e.trim());

  if (spl.length != 2 || isNaN(parseInt(spl[0])) || isNaN(parseInt(spl[1]))) {
    return;
  } else return parseInt(spl[0]) * 60 * 1000 + parseInt(spl[1]) * 1000;
};

export const trackToString = (
  track: TrackInfo,
  user?: User,
  loop?: boolean,
) => {
  return `[\`${track.title.replace(/`/g, '')}\`](<${track.uri}>) - ${
    track.author.replace(/`/g, '')
  } - \`${
    formatLength(
      track.length,
    )
  }\`${user ? ` - ${user.tag}${loop ? ' (Loop)' : ''}` : ''}`;
};

const BLUE_SQ = '🟦';
const WHITE_SQ = '⬜';
const PROG_LEN = 16;

export const createProgress = (c: number, m: number) => {
  let perc = (c / m) * 100;
  perc = (perc - (perc % PROG_LEN)) / PROG_LEN;
  return `\`${BLUE_SQ.repeat(perc < 0 ? 0 : perc)}${
    WHITE_SQ.repeat(
      PROG_LEN - perc < 0 ? 0 : PROG_LEN - perc,
    )
  }\``;
};

export const ytThumb = (id: string) => {
  return `https://img.youtube.com/vi/${id}/hqdefault.jpg`;
};

export interface TrackInfo {
  identifier: string;
  isSeekable: boolean;
  author: string;
  length: number;
  isStream: boolean;
  position: number;
  title: string;
  uri: string;
}

export interface QueueTrack {
  track: string;
  info: TrackInfo;
  by: User;
  loop?: boolean;
}

export class Queue {
  guild: Guild;
  tracks: Array<QueueTrack> = [];
  player: lava.Player;
  current: string | null = null;
  autoplay = true;
  loopqueue = false;
  _lastSkipped?: QueueTrack;

  constructor(guild: Guild, player: lava.Player) {
    this.guild = guild;
    this.player = player;

    this.player.on('trackStart', (track) => {
      if (this.current?.includes('soundboard')) {
        this.guild.client.setPresence({
          name: 'Sound Effects',
          type: 'LISTENING',
        });
        // log('Debug', 'Skipping start handler on soundboard');
        return;
      }

      this.current = track;
      if (this.tracks.length === 0) {
        return log('Track', 'Started but not in queue');
      }

      log('Track', `Start - ${this.tracks[0].info.title}`);

      if (this.guild.id == config.mainGuild) {
        this.guild.client.setPresence({
          name: this.tracks[0].info.title,
          type: 'LISTENING',
        });
      }
    });

    this.player.on('trackEnd', async () => {
      if (this.current == 'soundboard') {
        // log('Debug', 'Skipping end handler on sounboard init');
        this.current = 'soundboardskip';
        return;
      } else if (this.current == 'soundboardskip') {
        // log(
        //   'Debug',
        //   'Final soundboard event, return to paused & untouched queue',
        // );
        this.current = null;
        await this.play();
        await this.player.pause(true);
        return;
      }

      if (this.guild.id == config.mainGuild) {
        this.guild.client.setPresence({
          name: 'Music',
          type: 'LISTENING',
        });
      }

      this.current = null;
      const track = this.tracks.shift();
      log('Track', `End - ${track?.info.title}`);
      if (this.loopqueue && track !== undefined) {
        track.loop = true;
        this.tracks.push(track);
      }

      if (this._lastSkipped) {
        this._lastSkipped = undefined;
        this.player.pause(false);
        return;
      }

      if (this.autoplay) {
        this.play();
      }
    });
  }

  async enqueue(track: QueueTrack) {
    this.tracks.push(track);
    if (this.tracks.length === 1) await this.play();
    return this;
  }

  async search(q: string): Promise<Track[]> {
    return (await this.player.node.rest.loadTracks(q)).tracks;
  }

  async ytsearch(q: string): Promise<Track[]> {
    return await this.search(`ytsearch:${q}`);
  }

  async play(track?: QueueTrack): Promise<Queue> {
    if (this.tracks.length !== 0) {
      await this.player.play(track ?? this.tracks[0].track);
    }
    return this;
  }

  async skip(): Promise<Queue> {
    if (this.tracks[1] === undefined) return this;
    const track = this.tracks[1];
    this._lastSkipped = track;
    log('Track', `Skip - ${track.info.title}`);
    await this.play(track);
    return this;
  }
}

export class QueueManager {
  mod: MusicSlashModule;
  queues: Collection<string, Queue> = new Collection();

  constructor(mod: MusicSlashModule) {
    this.mod = mod;
  }

  get(guild: Guild | string): Queue | undefined {
    return this.queues.get(guild instanceof Guild ? guild.id : guild);
  }

  has(guild: Guild | string): boolean {
    return this.queues.has(guild instanceof Guild ? guild.id : guild);
  }

  add(guild: Guild): Queue {
    if (this.has(guild)) throw new Error('Guild already in Queue');
    const queue = new Queue(
      guild,
      this.mod.lava.createPlayer(BigInt(guild.id)),
    );

    const newvol = Deno.env.get('DEFAULT_VOLUME') as unknown as number;
    queue.player.setVolume(scale(newvol));
    this.queues.set(guild.id, queue);
    return queue;
  }

  remove(guild: Guild | string): boolean {
    if (!this.has(guild)) return false;
    else {
      return this.queues.delete(
        guild instanceof Guild ? guild.id : guild,
      );
    }
  }
}
