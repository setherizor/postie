import { EvalCommand } from './eval.ts';
import { CleanCommand } from './clean.ts';
import { PingCommand } from './ping.ts';
import { HelpCommand } from './help.ts';
import { UserinfoCommand } from './userinfo.ts';
import { SlashCommand } from './slash.ts';

export default [
  EvalCommand,
  CleanCommand,
  PingCommand,
  HelpCommand,
  UserinfoCommand,
  SlashCommand,
];
