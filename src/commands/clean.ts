import { Command, CommandContext } from '../../deps.ts';

export class CleanCommand extends Command {
  name = 'clean';
  description = 'delete the last 20 bot messages';
  aliases = ['c'];

  async execute(ctx: CommandContext) {
    const msgs = await ctx.channel.fetchMessages({
      limit: 20,
      before: ctx.message,
    });

    await Promise.all(
      msgs.filter((m) => m.author.bot! && m.author.id == ctx.client.user!.id)
        .map((m) => m.delete()),
    );

    const reply = await ctx.message.reply('I cleaned up recent bot messages');
    setTimeout(() => ctx.message.delete(), 1000);
    setTimeout(() => reply.delete(), 3000);
  }
}
