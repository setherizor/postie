import { Command, CommandContext, Embed } from '../../deps.ts';

export class HelpCommand extends Command {
  name = 'help';
  description = 'list commands and descriptions';
  guildOnly = true;
  aliases = ['h', '?'];

  async execute(ctx: CommandContext): Promise<void> {
    const prefix = ctx.client.getGuildPrefix(ctx.guild!.id) as string;
    const cmds = ctx.client.commands.list;
    const cmdsText = cmds.array().sort((a, b) => a.name.localeCompare(b.name))
      .map((c) =>
        `\`${
          c.toString().replace('Command: ', prefix)
        }\` - ${c.description} \`(${c.aliases})\``
      ).join('\n');

    const embed = new Embed()
      .setColor(0xff00ff)
      .setTitle(`Chat Commands`)
      .setDescription(cmdsText)
      .addField(
        'Slash Commands',
        'Type `/` to see a list of available slash commands',
      )
      .addField(
        'Bot Author',
        `<@298909920881934339> - [\`Website\`](https://sethp.cc)`,
      )
      .addField(
        'Permissions',
        'Server admin must configure roles allowed to interact with the bot',
      );

    await ctx.channel.send(embed);
  }
}
