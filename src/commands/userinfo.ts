import {
  Command,
  CommandContext,
  Embed,
  Member,
  PermissionFlags,
} from '../../deps.ts';

export class UserinfoCommand extends Command {
  name = 'userinfo';
  description = 'get information about yourself';
  guildOnly = true;
  aliases = ['u', 'user'];

  async execute(ctx: CommandContext): Promise<void> {
    const member: Member = ctx.message.member!;
    const roles = await member.roles.array();
    const embed = new Embed()
      .setTitle(`User Info`)
      .setAuthor({ name: member.user.tag })
      .addField('ID', member.id)
      .addField('Roles', roles.map((r) => r.name).join(', '))
      .addField(
        'Permissions',
        JSON.stringify(member.permissions.has(PermissionFlags.ADMINISTRATOR)),
      )
      .setColor(0xff00ff);
    ctx.channel.send(embed);
  }
}
