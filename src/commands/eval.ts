import { Command, CommandContext } from '../../deps.ts';

export class EvalCommand extends Command {
  name = 'eval';
  description = 'a debug command for bot owner only';
  aliases = ['e'];
  ownerOnly = true;

  async execute(c: CommandContext): Promise<void> {
    try {
      // eslint-disable-next-line no-eval
      let evaled = eval(c.argString);
      if (evaled instanceof Promise) evaled = await evaled;
      if (typeof evaled === 'object') evaled = Deno.inspect(evaled);
      await c.message.reply(
        `\`\`\`js\n${`${evaled}`.substring(0, 1990)}\n\`\`\``,
      );
    } catch (e) {
      c.message.reply(`\`\`\`js\n${(e as Error).stack}\n\`\`\``);
    }
  }
}
