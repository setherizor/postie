import { Args, Command, CommandContext } from '../../deps.ts';
import type { Postie } from '../bot.ts';

export class SlashCommand extends Command {
  name = 'slash';
  description = 'add bot\'s slash commands to guild';
  aliases = ['slashes'];
  args: Args[] = [
    {
      name: 'action',
      match: 'content',
    },
  ];

  onMissingArgs(ctx: CommandContext) {
    ctx.message.reply(
      'please add `add` or `remove` to the end of that command',
    );
  }

  async execute(ctx: CommandContext) {
    const p = ctx.client as Postie;
    const act = ctx.args!.action as string[];
    switch (act[0]) {
      case 'add': {
        const rep = await ctx.message.reply(
          'Starting to add my slash commands.',
        );
        await p.setupSlashCommands(ctx.guild!.id);
        await rep.edit('I added my slash commands!');
        return;
      }
      case 'remove': {
        const rep = await ctx.message.reply(
          'Starting to remove my slash commands.',
        );
        await p.setupSlashCommands(ctx.guild!.id, true);
        await rep.edit(
          'I unregisterd my slash commands (could take some time for Discord to show the updates)',
        );
        return;
      }
      default: {
        await this.onMissingArgs(ctx);
      }
    }
  }
}
