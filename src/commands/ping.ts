import { Command, CommandContext } from '../../deps.ts';

export class PingCommand extends Command {
  name = 'ping';
  description = 'check bots response time';
  aliases = ['p'];

  execute(ctx: CommandContext) {
    ctx.message.reply(`Pong! \`Latency: ${ctx.client.gateway.ping}ms\``);
  }
}
