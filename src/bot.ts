import {
  ApplicationCommandPartial,
  CommandClient,
  CommandClientOptions,
  event,
  GatewayIntents,
  Guild,
  Interaction,
  isMessageComponentInteraction,
  Message,
  MessageComponentInteraction,
  MessageReaction,
  TextChannel,
  User,
} from '../deps.ts';

import { MusicSlashModule } from './modules/music/mod.ts';
import { DefaultModule } from './modules/default/mod.ts';
import { log } from './util/log.ts';
import cmds from './commands/mod.ts';

import { Db } from '../common/db.ts';

export class Postie extends CommandClient {
  mainGuild: string | Guild = Deno.env.get('MAINGUILD') as string;
  sbshortcuts?: Record<string, string>;
  caseSensitive = true;

  db: Db;
  modules: {
    default: DefaultModule;
    music: MusicSlashModule;
  };

  // This will output shard communications
  debug(title: string, msg: string): void {
    if (Deno.env.get('POSTIE_DEBUG') != 'true') {
      return;
    }

    console.log(`[${title}] ${msg}`);
    if (title === 'Gateway' && msg === 'Initializing WebSocket...') {
      try {
        throw new Error('Stack');
      } catch (e) {
        console.log(e.stack);
      }
    }
  }

  constructor(options: CommandClientOptions) {
    super(
      Object.assign(options, {
        // https://discord.com/developers/docs/topics/gateway#list-of-intents
        intents: [
          GatewayIntents.GUILDS,
          GatewayIntents.GUILD_MODERATION,
          GatewayIntents.GUILD_EMOJIS_AND_STICKERS,
          GatewayIntents.GUILD_VOICE_STATES,
          // GatewayIntents.GUILD_PRESENCES,
          GatewayIntents.GUILD_MESSAGES,
          GatewayIntents.GUILD_MESSAGE_REACTIONS,

          GatewayIntents.DIRECT_MESSAGES,
          GatewayIntents.DIRECT_MESSAGE_TYPING,
          GatewayIntents.DIRECT_MESSAGE_REACTIONS,
        ],
        presence: {
          name: 'the latest updates',
          type: 'STREAMING',
        },
      }),
    );

    this.fetchUncachedReactions = true;

    // Register normal commands
    for (const c of cmds) {
      this.commands.add(c);
    }

    this.db = new Db(false);

    this.modules = {
      default: new DefaultModule(this),
      music: new MusicSlashModule(this),
    };

    Object.values(this.modules).forEach((module) =>
      this.interactions.loadModule(module)
    );

    this.interactions.getHandlers();
  }

  @event()
  async ready() {
    log('Bot', `Logged in as ${this.user?.tag}!`);

    this.modules.music.lava.on('nodeConnect', (node) => {
      log('Lava', `Connected to node:${node.id}!`);
    });
    this.modules.music.lava.on('nodeDisconnect', (node, _n, reason) => {
      log('Lava', `Socket Closed - node:${node.id} - reason: ${reason}`);
    });
    // Useful dor debugging
    // this.modules.music.lava.on('nodeMessage', (node, e) => {
    //   log('Lava', `Socket message - node: ${node.id} - ${JSON.stringify(e)}`);
    // });
    this.modules.music.lava.on('nodeError', (e, err) => {
      log('Lava', `Socket Error [node:${e.id}] ${err.message}`);
    });

    // Lavalink takes some time to get started using docker
    setTimeout(
      () => {
        this.modules.music.lava.init(this.user?.snowflake.snowflake);

        this.setPresence({
          name: 'Music',
          type: 'LISTENING',
        });
      },
      30 * 1000,
    );

    await this.setupReactionMessage();
    // await this.setupSlashCommands(this.mainGuild as string);
  }

  // Slash commands setup
  // TODO: the behaivor without passing a guild id seems a bit odd
  async setupSlashCommands(guildID: string, remove = false) {
    // Unregister guild commands
    if (remove) {
      const cmds = await this.interactions.commands.bulkEdit([]);
      log(
        'Slash',
        `Removed slash commands from guild ${guildID}! ${cmds.array()}`,
      );
      return;
    }

    // Setup the current slash commands
    const allCommandsDefs = Object.values(this.modules).reduce(
      (a, m) => a.concat(m.definitions),
      [] as ApplicationCommandPartial[],
    );
    await this.interactions.commands.bulkEdit(allCommandsDefs);
    log('Slash commands have been created');
  }

  @event()
  async interactionCreate(i: Interaction) {
    if (isMessageComponentInteraction(i)) {
      const d = i as MessageComponentInteraction;

      switch (d.customID.split('::')[0]) {
        case 'sb':
        case 'media':
          await this.modules.default.soundboardInteractionHandler(d, this);
          break;

        case 'editrr':
          await this.modules.default.editReactionRolesInteractionHandler(d);
          break;

        default:
          d.respond({
            content: 'Something is not quite right with that interaction',
            ephemeral: true,
          });
      }

      if (!d.responded) {
        d.respond({ type: 7 }); // PONG
      }
    }
  }

  // https://doc.deno.land/https/deno.land/x/harmony/mod.ts#ClientEvents
  @event()
  guildCreate(guild: Guild) {
    log(`guild joined: ${guild.name} (${guild.id}) `);
  }

  @event()
  guildDelete(guild: Guild) {
    log(`guild left: ${guild.name} (${guild.id}) `);
  }

  // Reaction roles setup
  async setupReactionMessage(quiet = false) {
    const dbGuilds = this.db.get(`guilds`);

    for (const g of Object.keys(dbGuilds)) {
      if (!quiet) {
        log('Bot Setup', 'caching guild ' + g);
      }
      const reactionMessages = dbGuilds[g].reactionMessages;

      if (!reactionMessages) {
        continue;
      }

      // Handle edge case of multiple reaction messages being in one channel
      const alreadyFetched: string[] = [];

      // Not sure if this caching actually works properly
      for (const r of Object.keys(reactionMessages)) {
        try {
          const parts = r.split('@');
          const c = await this.channels.resolve(parts[0]!) as TextChannel;

          if (!alreadyFetched.includes(parts[0]!)) {
            await c.messages.flush();
          }

          const m = await c.messages.resolve(parts[1]!);
          await m!.reactions.fetch();

          if (!quiet) {
            log('Bot Setup', 'caching reaction message ' + r);
          }
        } catch (_error) {
          this.db.set(`guilds.${g}.reactionMessages.${r}`, null);
          if (!quiet) {
            log('Bot Setup', 'clearing missing reaction message ' + r);
          }
        }
      }
    }
  }

  @event()
  messageReactionAdd(reaction: MessageReaction, reactor: User) {
    this.handleReactions(reaction, reactor, false);
  }

  // @event()
  // messageReactionRemove(reaction: MessageReaction, reactor: User) {
  //   this.handleReactions(reaction, reactor, true);
  // }

  // Added this because removing reactions did not always trigger the normal event
  @event('raw')
  // deno-lint-ignore no-explicit-any
  async test(type: any, data: any) {
    if (type == 'MESSAGE_REACTION_REMOVE') {
      this.handleReactions(
        {
          // @ts-expect-error TODO: rafactor if delete becomes more typical
          message: {
            id: data.message_id,
            guildID: data.guild_id,
            channelID: data.channel_id,
            guild: await this.guilds.fetch(data.guild_id),
          },
          emoji: data.emoji,
        },
        {
          id: data.user_id,
          username: 'ID:' + data.user_id,
        },
        true,
      );
    }
  }

  async handleReactions(
    reaction: MessageReaction,
    reactor: User,
    removeRole: boolean,
  ) {
    const { message, emoji } = reaction;

    if (this.user?.id == reactor.id) {
      return;
    }
    // Pass off to the editor, then continue if need be
    const skip = await this.modules.default.editReactionRolesReactionHandler(
      reaction,
      reactor,
    );

    if (skip) {
      return;
    }

    const reactionMessages = this.db.get(
      `guilds.${message.guildID}.reactionMessages.${message.channelID}@${message.id}`,
    );

    if (!reactionMessages) {
      return;
    }

    // Have to do this because of how custom emojis work in the API
    const key = Object.keys(reactionMessages)
      .map(atob)
      .find((k) => k.startsWith(encodeURIComponent(emoji.name!)));

    const roleId = reactionMessages[btoa(key!)];

    if (!roleId) return;

    try {
      const mem = (await message.guild!.members.resolve(reactor.id))!;
      const role = (await message.guild?.roles.get(roleId))!;

      if (removeRole) {
        log('Bot', `removing role: ${role?.name} from ${reactor.username}`);
        await mem.roles.remove(role); // 'reaction roled remove interaction');
      } else {
        log(`adding role: ${role?.name} to user ${reactor.username}`);
        await mem.roles.add(role, 'reaction roles add interaction');
      }
    } catch (error) {
      log('reaction role error: ' + error);
    }
  }

  @event()
  messageDelete(msg: Message) {
    // only run on guild messages
    if (msg.guildID) {
      const reactionMessages = Object.keys(
        this.db.get(`guilds.${msg.guildID}.reactionMessages`) ?? {},
      );
      const key = msg.channelID + '@' + msg.id;
      if (
        reactionMessages &&
        reactionMessages.length != 0 &&
        reactionMessages.indexOf(key) != -1
      ) {
        log(`deleting reactionMessage ${msg.id} from guild ${msg.guildID}`);
        this.db.set(`guilds.${msg.guildID}.reactionMessages.${key}`, null);
      }
    }
  }

  @event('error')
  @event('commandError')
  @event('gatewayError')
  error(err: Error) {
    console.error(err); // or your preferred logger
  }
}
