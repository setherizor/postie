import {
  ApplicationCommandInteraction,
  CommandClient,
  PermissionFlags,
} from '../../deps.ts';
import { Db } from '../../common/db.ts';
import { log } from './log.ts';

const db = new Db();

export const isBotMaster = async (i: ApplicationCommandInteraction) => {
  try {
    const masters = Object.keys(
      db.get(`guilds.${i.guild!.id}.botMasters`) || {},
    );

    if (!masters) {
      log('Permissions', 'no masters found for guild');
      return Boolean(false);
    }
    const m = i.member!;
    const userRoles = (await m.roles.array())!;
    const perms = m.permissions.has(PermissionFlags.ADMINISTRATOR);

    // it bot owner, server admin, or has a botmaster role from the web UI
    const isBotMaster = userRoles.some((r) => masters.includes(r.id)) ||
      perms ||
      (i.client as CommandClient).owners!.includes(m.id);

    return Boolean(isBotMaster);
  } catch (error) {
    log('Permissions', error);
    return Boolean(false);
  }
};
